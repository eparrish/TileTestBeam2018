"""
TODO: Write UploadToDB function
Author: Elliot Parrish
Date: May 29, 2018
Script to grab position information of Tile Test Beam table and input into run database.

Notes:
	logfile with position information is in 192.168.0.1 /home/table/build-TileTableCtrl-Dekstop-Debug/EncodersCoordinate.txt
		need password to acces??
		ssh table@128.141.148.41 "tail /home/table/build-TileTableCtrl-Desktop-Debug/EncodersCoordinate.txt"
			pw:on_monitor

"""
from __future__ import division
import datetime
import subprocess
import csv
import os
import argparse

def GetArguments():
	"""
	Function to get the commandline input for the run number
	inputs: 	None
	outputs: 	run_num 		- integer identifier for the run to consider
	"""
	parser=argparse.ArgumentParser()
	parser.add_argument("-sr", "--runnumber", type=int, required=False, dest="run_num", help="Single run number to update")
	parser.add_argument("-br", "--beginingrun", type=int, required=False, dest="start_run", help="Run number to start from")
	parser.add_argument("-er", "--endrun", type=int, required=False, dest="end_run", help="Run number to end with")
	parser.add_argument("-all", "-allruns", required=False, dest="all_runs", help="Option to look at all runs", action="store_true")
	args = parser.parse_args()
	if not args.run_num and not args.start_run and not args.end_run and not args.all_runs:
		raise Exception("Please specify a run mode to use. Use --help for explanation")
	if args.run_num and (args.start_run or args.end_run or args.all_runs):
		raise Exception("Please only specify one run mode to use. Use --help for explanation")
	if args.all_runs and (args.start_run or args.end_run or args.run_num):
		raise Exception("Please only specify one run mode to use. Use --help for explanation")
	if (args.start_run and not args.end_run) or (args.end_run and not args.start_run):
		raise Exception("Please specify both a start and end run number. Use --help for explanation")

	arguments = {
		"run_num":		args.run_num,
		"start_run":	args.start_run,
		"end_run":		args.end_run,
		"all_runs":		args.all_runs,
	}
	return arguments

def FixFormatofData(FileToRead):
	"""
	Function to strip lines of data and put in readable format
	inputs: 	FileToRead 		- opened text file that contains the position information
	outputs: 	strippedlines 	- list containting the position data ready to be parsed
	"""
	textlines = FileToRead.readlines()
	strippedlines = []
	for line in textlines:
		line = filter(None,line.rsplit())
		# print line
		# try:
			# line.remove("::")
		# except:
			# print line
			# pass
		strippedlines.append(line)
	return strippedlines

def GetRunDbInfo():
	output_unreadable = subprocess.Popen("mysql -h pcata007 -u reader -e 'select run,date,enddate from tile.runinfo where run>=810000 and date>2018-01-01' >| %s/run_info.csv" %(os.getcwd()), shell=True, stdout=subprocess.PIPE)
	output_unreadable.wait()
	return

def GetRunList():
	run_list = []
	with open(os.getcwd()+'/run_info.csv', 'rb') as csvfile:
		mycsvfile = list(csv.reader(csvfile, delimiter='	'))
		for runinfo in mycsvfile:
			if runinfo[0] == 'run': continue;
			if int(runinfo[0]) > 9999999: continue;
			run_list.append(int(runinfo[0]))
	return run_list

def GetRunTime(run_number):
	"""
	Function to get start and end time of run from pcata007.cern.ch db
	inputs: 	run_number 		- an integer value used to grab the run start and end times
	outputs: 	run number 		- an integer value identifying the run
				start_time 		- a datetime.datetime object that holds the start date/time of the run
				end_time 		- a datetime.datetime object that holds the end date/time of the run
	"""
	with open(os.getcwd()+'/run_info.csv', 'rb') as csvfile:
		mycsvfile = list(csv.reader(csvfile, delimiter='	'))
		for runinfo in mycsvfile:
			if runinfo[0] == 'run': continue;
			if int(runinfo[0]) != run_number: continue;
			end_time = datetime.datetime.strptime( runinfo[2], "%Y-%m-%d %H:%M:%S" )
			if end_time.year < 2018: continue;
			runnumber = int(runinfo[0])
			start_time = datetime.datetime.strptime( runinfo[1], "%Y-%m-%d %H:%M:%S" )
	try:
		return run_number, start_time, end_time
	except:
		raise Exception("Check run number. No information in database for run %s" %(run_number))

def IsTimeStampInRun(start_time, end_time, time_stamp):
	"""
	Function that contains the logic to determine if a timestamp is within the given start and end time range.
	inputs: 	start_time 		- a datetime.datetime object that holds the start date/time of the run
				end_time 		- a datetime.datetime object that holds the end date/time of the run
				time_stamp 		- a datetime.datetime object that holds the date/time of the position information to be considered
	outputs:
				boolean value
	"""
	if start_time <= end_time:
		return start_time <= time_stamp <= end_time
	else:
		return start_time <= time_stamp or time_stamp <= end_time

def UploadToDB(run_num, module, tile, dy, dz, theta):
	"""
	TODO: Write function
	Function that uploads temporary dqlite file to database
	"""
	upload_process = subprocess.Popen("mysql -h pcata007 -u tilebeam --password=datarun95 -e 'update tile.runinfo set module=%s,cell=%s,y=%s,z=%s,theta=%s where run=%s'"%(module,tile,dy,dz,theta,run_num), shell=True, stdout=subprocess.PIPE)
	upload_process.wait()
        # print "mysql -h pcata007 -u tilebeam --password=datarun95 -e 'update tile.runinfo set module=%s,cell=%s,y=%s,z=%s,theta=%s where run=%s'"%(module,tile,dy,dz,theta,run_num)
	return 

def ParsePositionLogFile(FileToRead, arguments):
	"""
	TODO: pass arguments to UploadToDB
	Main function of script
	inputs: 	FileToRead 		- an opened text file that contains the position information from log file
				run_num 		- an integer value identifying to the run to consider
	outputs:
				None
	"""
	readableData = FixFormatofData(FileToRead)

	GetRunDbInfo()

	if arguments["run_num"]:
		runs_to_check = xrange(arguments["run_num"], arguments["run_num"]+1)
	elif arguments["start_run"] and arguments["end_run"]:
		runs_to_check = xrange(arguments["start_run"],arguments["end_run"]+1)
	elif arguments["all_runs"]:
		runs_to_check = GetRunList()
	else:
		raise Exception("Arguments not parsed correctly. Use --help for explanation")

	for run_num in runs_to_check:

		thisruninfo = GetRunTime(run_num)
		runPositionInfo = [[],[],[],[], []]

		for entry in xrange(len(readableData)):
			if readableData[entry][5] == '1': continue; #check that the table is not moving
                        if "Start" in readableData[entry]: continue;
                        if "eta" in readableData[entry]: continue;
			time_stamp =  datetime.datetime.strptime( readableData[entry][0]+":"+readableData[entry][1], "%Y-%m-%d:%H:%M:%S" )
			theta = readableData[entry][8]
			module = readableData[entry][9]
			tile = readableData[entry][11]
			dz = readableData[entry][15]
			dy = readableData[entry][16]
                        # print thisruninfo[1], thisruninfo[2], time_stamp
			if IsTimeStampInRun(thisruninfo[1], thisruninfo[2], time_stamp):
                                # print readableData[entry]
				runPositionInfo[0].append(module)
				runPositionInfo[1].append(tile)
				runPositionInfo[2].append(float(dy))
				runPositionInfo[3].append(float(dz))
				runPositionInfo[4].append(float(theta))
				time_stamp = None
			else:
				time_stamp = None
				continue

		if len(runPositionInfo[0]) == 0:
			print "No position information found for run %s. Check database and logfile." %(run_num)
			continue
			# raise Exception("No position information found for run %s. Check database and logfile." %(run_num))
		else:
			module = str(runPositionInfo[0][0])
			tile = runPositionInfo[1][0]
			avg_dy = sum(runPositionInfo[2])/len(runPositionInfo[2])
			avg_dz = sum(runPositionInfo[3])/len(runPositionInfo[3])
			avg_theta = sum(runPositionInfo[4])/len(runPositionInfo[4])

			print "Updating run db with run=%s module=%s tile=%s dy=%s dz=%s theta=%s" %(run_num, module, tile, avg_dy, avg_dz, avg_theta)
			# UploadToDB(run_num, module, tile, avg_dy, avg_dz, avg_theta)

	return

if __name__ == "__main__":
	# testfilepath = "/afs/cern.ch/user/e/eparrish/TileTestBeam2018/" #For lxplus
	# testfilepath = "/home/table/build-TileTableCtrl-Desktop-Debug/" #for pc7600nr3
	testfilepath = "/home/tiledaq/commissioning/statTB/table/build-TileTableCtrl-Desktop-Debug/" #for pcata007
	if testfilepath[-1] != "/":
		testfilepath += "/"
	else: pass;
	with open(testfilepath+"EncodersCoordinate.txt", "r") as FileToRead:
		ParsePositionLogFile(FileToRead, GetArguments())
